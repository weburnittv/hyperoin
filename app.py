from hyperoin import app, manager

app.run(debug=True, host="0.0.0.0")


@manager.command
def test():
    import unittest
    tests = unittest.TestLoader().discover('tests/hyperoin', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1
