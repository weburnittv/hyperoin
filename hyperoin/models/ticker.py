from sqlalchemy import Column, Integer, String, DateTime, func
from hyperoin.db import Base


class Ticker(Base):
    __tablename__ = 'tickers'
    id = Column(Integer, primary_key=True)
    symbol = Column(String(64), unique=False)
    market = Column(String(12), unique=False)
    time = Column(DateTime, default=func.now())

    def __init__(self, symbol=None, market=None):
        self.symbol = symbol
        self.market = market

    def __repr__(self):
        return '<Market %r>' % (self.market)
