from flask import Blueprint
from hyperoin.models.ticker import Ticker
from hyperoin.db import db_session

home = Blueprint('home', __name__)


@home.route("/friend/<string:friend>", methods=["GET"])
def home_index(friend):
    return "Hello Friend" + friend


@home.route("/ticker/<string:market>/<string:symbol>", methods=['POST'])
def create_ticker(market, symbol):
    db_session.add(Ticker(symbol, market))
    db_session.commit()
    return ''
