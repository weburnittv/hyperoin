from flask import Flask
from flask_restful import Api
from flask_migrate import Migrate
from flask_script import Manager
import os
from hyperoin.db import init_db, engine, db_config, Base
from hyperoin.http.home import home

init_db()
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = db_config
app.config.from_object(os.environ.get('ENV_CONFIG'))
migrate = Migrate(app, Base)
manager = Manager(app)
manager.run()
app.register_blueprint(home, prefix='/')
