import unittest
from core.MetaData import markets
from core.Services import Bitfinex, Bittrex


class TestMarkets(unittest.TestCase):
    def test_bitfinex_pair_selector(self):
        markets.MARKET_SYMBOLS[Bitfinex.MARKET] = ['ltcbtc', 'ethbtc']
        self.assertEqual('ltcbtc', markets.bitfinex_pair_selector('BTC-LTC'))
        self.assertEqual('ltcbtc', markets.bitfinex_pair_selector('LTC-BTC'))
        self.assertEqual('ethbtc', markets.bitfinex_pair_selector('ETH-BTC'))
        self.assertEqual('ethbtc', markets.bitfinex_pair_selector('BTC-ETH'))
        self.assertIsNone(markets.bitfinex_pair_selector('BTC-NEO'))


if __name__ == '__main__':
    unittest.main()
