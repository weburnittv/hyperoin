from setuptools import setup

setup(
    name='hyperoin',
    packages=['hyperoin'],
    include_package_data=True,
    install_requires=[
        'flask',
        'flask-script',
        'Flask-SQLAlchemy',
        'Flask-Migrate',
        'flask-restful',
        'marshmallow',
        'marshmallow-SQLAlchemy',
        'flask-marshmallow'
    ],
)
