from . import base as Base
from . import _bittrex

MARKET = 'bittrex'


class Bittrex(Base.ExchangeService):
    def __init__(self, api_key, api_secret):
        self.client = _bittrex.BittrexAPI(api_key, api_secret)

    def buy(self, amount, currency, bid):
        return self.client.buy_limit(market=currency, quantity=amount, rate=bid)

    def sell(self, amount, currency, bid):
        return self.client.sell_limit(market=currency, quantity=amount, rate=bid)

    def stat(self, major, minor):
        pass

    def markets(self):
        results = self.client.get_markets()
        markets = []
        for item in results['result']:
            markets.append(Base.Market(item['MarketName']))
        return markets

    def ticker(self, market):
        result = self.client.get_ticker(market)
        return Base.Ticker(result['result']['Bid'], result['result']['Ask'], result['result']['Last'])

    def account(self, currency):
        return self.client.get_balance(currency)

    def order(self, currency, amount, bid):
        return self.client.buy_limit(currency, amount, bid)
