import krakenex
from . import base as Base

MARKET = 'kraken'


class Kraken(Base.ExchangeService):
    def __init__(self, api_key, api_secret):
        self.tickers = {}
        self.api = krakenex.API(api_key, api_secret)

    def ticker(self, market):
        ticker = self.api.query_public('Ticker', req={'pair': market})
        for info, key in ticker.items():
            return Base.Ticker(float(info['a'][0]), float(info['b'][0]), float(ticker['c'][0]))

    def markets(self):
        items = self.api.query_public('AssetPairs')
        markets = []
        for symbol, market in items['result'].items():
            markets.append(Base.Market(market['altname']))
        return markets
