import poloniex
from . import base as Base

MARKET = 'poloniex'


class Poloniex():
    def __init__(self, key, secret):
        self.tickers = {}
        self.api = poloniex.Poloniex(key, secret)

    def ticker(self, market):
        if len(self.tickers) == 0:
            self.get_tickers()
        ticker = self.tickers[market]
        return Base.Ticker(float(ticker['highestBid']), float(ticker['lowestAsk']), float(ticker['last']))

    def get_tickers(self):
        if len(self.tickers) == 0:
            self.tickers = self.api.returnTicker()

    def markets(self):
        if len(self.tickers) == 0:
            self.get_tickers()
        markets = []
        items = self.tickers.keys()
        for market in items:
            markets.append(Base.Market(market))
        return markets
