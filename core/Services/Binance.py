from . import base as Base
from binance.client import Client

MARKET = 'binance'


class Binance(Base.ExchangeService):
    def __init__(self, key, secret):
        self.api = Client(key, secret)

    def stat(self, major, minor):
        pass

    def buy(self, amount, currency, bid):
        results = self.api.create_order(side="buy", type="market", quantity=amount, symbol=currency)
        return results

    def sell(self, amount, currency, bid):
        pass

    def order(self, currency, amount, bid):
        pass

    def account(self, currency):
        pass

    def markets(self):
        result = self.api.get_products()
        markets = []
        for item in result['data']:
            markets.append(Base.Market(item['symbol']))
        return markets

    def ticker(self, market):
        result = self.api.get_ticker(symbol=market)
        return Base.Ticker(float(result['bidPrice']), float(result['askPrice']), float(result['lastPrice']))
