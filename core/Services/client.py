import requests
import json

class HttpClient:
    def __init__(self, base_url, auth={}):
        self.base_url = base_url
        self.auth = auth

    def post(self, uri, request, **kwargs):
        self.auth.update(request.header)
        kwargs.setdefault('headers', self.auth)
        kwargs.setdefault('json', True)
        response = requests.post(self.base_url + '/' + uri, request.data, **kwargs)
        return json.loads(response.content)

    def get(self, uri, request, **kwargs):
        self.auth.update(request.header)
        kwargs.setdefault('headers', self.auth)
        kwargs.setdefault('json', True)
        response = requests.get(self.base_url + '/' + uri, request.data, **kwargs)
        return json.loads(response.content)

    def send(self, request):
        if request.method == 'GET':
            return self.get(request.uri, request)
        else:
            return self.post(request.uri, request)

class HttpRequest:
    def __init__(self, uri, data, header={}, method="GET"):
        self.uri = uri
        self.data = data
        self.header = header
        self.method = method
        pass
