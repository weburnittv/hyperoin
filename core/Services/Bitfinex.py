from . import base as Base
from . import _bitfinex

MARKET = 'bitfinex'


class Bitfinex(Base.ExchangeService):
    def __init__(self, api_key, api_secret):
        self.api = _bitfinex.BitfinexAPI(api_key, api_secret)
        self.client = _bitfinex.BitfinexClient()
        self.public = _bitfinex.BitfinexClient()

    def buy(self, amount, currency, bid):
        currency = currency.replace("-", "")
        return self.api.place_order(amount, bid, symbol=currency, side="buy", ord_type="fill-or-kill")

    def sell(self, amount, currency, bid):
        currency = currency.replace("-", "")
        return self.api.place_order(amount, bid, symbol=currency, side="buy", ord_type="fill-or-kill")

    def stat(self, major, minor):
        pass

    def markets(self):
        results = self.client.symbols_details()
        markets = []
        for item in results:
            markets.append(Base.Market(item['pair']))
        return markets

    def ticker(self, market):
        market = market.replace('-', '').lower()
        result = self.public.ticker(market)
        return Base.Ticker(result['bid'], result['ask'], result['last_price'])

    def account(self, currency):
        return self.api.history(currency)
