class ExchangeService:
    def __init__(self, **kwargs):
        pass

    def stat(self, major, minor):
        pass

    def buy(self, amount, currency, bid):
        pass

    def sell(self, amount, currency, bid):
        pass

    def order(self, currency, amount, bid):
        pass

    def account(self, currency):
        pass

    def markets(self):
        pass

    def ticker(self, market):
        pass


class Ticker:
    def __init__(self, bid, ask, last):
        self.bid = bid
        self.ask = ask
        self.last = last


class Market:
    def __init__(self, name):
        self.name = name
