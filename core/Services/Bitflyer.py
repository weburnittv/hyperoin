from . import _bitflyer
from . import base as Base

MARKET = 'bitflyer'


class Bitflyer(Base.ExchangeService):
    def __init__(self, api_key, api_secret):
        self.tickers = {}
        self.api = _bitflyer.API(api_key, api_secret)

    def ticker(self, market):
        ticker = self.api.ticker(product_code=market)
        return Base.Ticker(float(ticker['best_bid']), float(ticker['best_ask']), float(ticker['ltp']))

    def markets(self):
        items = self.api.markets()
        markets = []
        for market in items:
            markets.append(Base.Market(market['product_code']))
        return markets
