from core.Services import Bitfinex, Bittrex, Binance, Poloniex, Bitflyer, Kraken
import utils

MARKET_SYMBOLS = {}

EXCHANGES = [
    Bittrex.MARKET,
    Bitfinex.MARKET,
    Binance.MARKET,
    Poloniex.MARKET,
    Bitflyer.MARKET,
    Kraken.MARKET
]


def exchange_builder(name, key, secret):
    if name == Bitfinex.MARKET:
        return Bitfinex.Bitfinex(key, secret)
    elif name == Bittrex.MARKET:
        return Bittrex.Bittrex(key, secret)
    elif name == Binance.MARKET:
        return Binance.Binance(key, secret)
    elif name == Poloniex.MARKET:
        return Poloniex.Poloniex(key, secret)
    elif name == Bitflyer.MARKET:
        return Bitflyer.Bitflyer(key, secret)
    elif name == Kraken.MARKET:
        return Kraken.Kraken(key, secret)


def bitfinex_pair_selector(market):
    left, right = market.split('-')
    try_me = left.lower() + right.lower()
    if MARKET_SYMBOLS[Bitfinex.MARKET].count(try_me):
        return try_me
    try_me = right.lower() + left.lower()
    if MARKET_SYMBOLS[Bitfinex.MARKET].count(try_me):
        return try_me
    return None


def binance_pair_selector(market):
    left, right = market.split('-')
    try_me = left + right
    if MARKET_SYMBOLS[Binance.MARKET].count(try_me):
        return try_me
    try_me = right + left
    if MARKET_SYMBOLS[Binance.MARKET].count(try_me):
        return try_me
    return None


def poloniex_pair_selector(market):
    left, right = market.split('-')
    try_me = left + '_' + right
    if MARKET_SYMBOLS[Poloniex.MARKET].count(try_me):
        return try_me
    try_me = right + '_' + left
    if MARKET_SYMBOLS[Poloniex.MARKET].count(try_me):
        return try_me
    return None


def bitflyer_pair_selector(market):
    left, right = market.split('-')
    try_me = left + '_' + right
    if MARKET_SYMBOLS[Bitflyer.MARKET].count(try_me):
        return try_me
    try_me = right + '_' + left
    if MARKET_SYMBOLS[Bitflyer.MARKET].count(try_me):
        return try_me
    return None


def kraken_pair_selector(market):
    left, right = market.split('-')
    try_me = left.lower() + right.lower()
    if MARKET_SYMBOLS[Kraken.MARKET].count(try_me):
        return try_me
    try_me = right.lower() + left.lower()
    if MARKET_SYMBOLS[Kraken.MARKET].count(try_me):
        return try_me
    return None


def selectors(exchange):
    return {
        Bitfinex.MARKET: bitfinex_pair_selector,
        Binance.MARKET: binance_pair_selector,
        Poloniex.MARKET: poloniex_pair_selector,
        Bitflyer.MARKET: bitflyer_pair_selector,
        Kraken.MARKET: kraken_pair_selector,
        Bittrex.MARKET: lambda x: x
    }[exchange]


def add_market(market, symbol):
    if MARKET_SYMBOLS.get(market) is None:
        MARKET_SYMBOLS[market] = []
    MARKET_SYMBOLS[market].append(symbol)


def get_market(market):
    return MARKET_SYMBOLS[market]


def get_pairs():
    pass


class MarketCoordinator:
    def __init__(self, config):
        self.config = config
        self.exchanges = EXCHANGES

    def crawl_markets(self, exchanges=None):
        if exchanges is not None:
            self.exchanges = exchanges
        pairs = self.market_pairs()
        tickers = []
        for pair in pairs:
            tickers.append(self.__stats(pair))
        return tickers

    def clients(self):
        clients = []
        for exchange in self.exchanges:
            exchange_service = exchange_builder(
                exchange,
                self.config[exchange]['key'],
                self.config[exchange]['secret']
            )
            clients.append(exchange_service)
        return clients

    def market_pairs(self):
        self.__symbols()
        base_symbols = get_market(Bittrex.MARKET)
        results = []
        for symbol in base_symbols:
            pairs = []
            for exchange in self.exchanges:
                pair = selectors(exchange)(symbol)
                if pair is not None:
                    coin = Coin(exchange, pair)
                    pairs.append(coin)
            if len(pairs) > 1:
                results.append(pairs)
        return results

    def __stats(self, pairs):
        results = []
        for pair in pairs:
            ticker = exchange_builder(pair.exchange, self.config[pair.exchange]['key'],
                                      self.config[pair.exchange]['secret']).ticker(pair.symbol)
            results.append(Market(pair.exchange, pair.symbol, ticker))
        return results

    def __symbols(self):
        for exchange in self.exchanges:
            exchange_service = exchange_builder(
                exchange,
                self.config[exchange]['key'],
                self.config[exchange]['secret']
            )
            markets = exchange_service.markets()
            for market in markets:
                add_market(exchange, market.name)
        return MARKET_SYMBOLS


class Market:
    def __init__(self, exchange, symbol, ticker):
        self.exchange = exchange
        self.symbol = symbol
        self.ticker = ticker


class Pair:
    def __init__(self, mother, father):
        self.mother = mother
        self.father = father


class Coin:
    def __init__(self, exchange, symbol):
        self.exchange = exchange
        self.symbol = symbol
