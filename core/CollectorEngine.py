from core.Services import Bitfinex, Bittrex, Binance
import core.MetaData.markets as Market
import numpy as np
import utils
import core.Mining.Dealer as Broker
import core.Mining.Tracker as mining
import random, sys, time
from multiprocessing import Lock, Process, Queue

DEALER_P = 2  # the number of processes we want to launch
DEALER_J = 1  # the number of jobs we want to process


def expose_market(bid_market, ask_market, profit, percentage):
    if profit > 0:
        print('Bid(%s) Exchange(%s): Ask(%f), Bid(%f), Last(%f)  ' % (
            bid_market.exchange, bid_market.symbol, bid_market.ticker.ask,
            bid_market.ticker.bid,
            bid_market.ticker.last))
        print('Ask(%s) Exchange(%s): Ask(%f), Bid(%f), Last(%f)  ' % (
            ask_market.exchange, ask_market.symbol, ask_market.ticker.ask,
            ask_market.ticker.bid,
            ask_market.ticker.last))
        print("Profit percent: " + percentage)


# slave function
def deal(procID, jobs, dispLock, currency, amount, buyer, seller, bid, ask):
    try:
        while True:
            jobData = jobs.get_nowait()
            dispLock.acquire()
            sys.stdout.write('slave process %d ' % procID)
            sys.stdout.write('working in job %d\n' % jobData['jobID'])
            sys.stdout.flush()
            dispLock.release()

            dealer = Broker.Dealer(currency, amount)
            dealer.buy(buyer, bid)
            dealer.sell(seller, ask)
    except:
        pass  # an exception is raised when job queue is empty


class Engine:
    def __init__(self, config):
        self.config = config
        self.exchanges = [Bittrex.MARKET, Bitfinex.MARKET, Binance.MARKET]
        self.market = Market.MarketCoordinator(config)
        self.notes = []
        self.budget = 1000
        self.trackers = []

    def analyzer(self, exchanges=None, force_update=False, dispatch_market=expose_market):
        if exchanges is not None:
            self.exchanges = exchanges
        if force_update or len(self.notes) == 0:
            self.notes = self.market.crawl_markets(self.exchanges)
        for pairs in self.notes:
            sells = []
            buys = []
            for pair in pairs:
                sells.append(pair.ticker.ask)
                buys.append(pair.ticker.bid)
            max_bid = max(buys)
            min_ask = min(sells)
            is_potential = max_bid > min_ask
            if is_potential:  # potential
                max_bid_index = buys.index(max_bid)
                bid_market = pairs[max_bid_index]
                bid_fee = self.config[bid_market.exchange]['fee']
                min_ask_index = sells.index(min_ask)
                ask_market = pairs[min_ask_index]
                ask_fee = self.config[ask_market.exchange]['fee']
                total_fee = max_bid * bid_fee + min_ask * ask_fee
                profit = max_bid - min_ask - total_fee
                is_profitable = profit > 0
                percentage = str(profit / (max_bid + min_ask + total_fee) * 100)
                if is_profitable:
                    dispatch_market(bid_market, ask_market, profit, percentage)

    def monitor(self, exchanges, config):
        clients = self.market.clients()
        trackers = []
        for exchange in exchanges:
            for client in clients:
                trackers.append(mining.Tracker(client, exchange, config))
        self.trackers = trackers
        for tracker in self.trackers:
            tracker.run(self.ticker_broadcast)

    def ticker_broadcast(self, tracker, ticker):
        print('Process a Tracker: %s - %s', tracker.market, ticker.last)
        self.budget -= ticker.last
        if self.budget < 100:
            for tracker in self.trackers:
                tracker.stop()

    # master process entry logic
    def processing(self):
        pool = []  # instantiate pool of processes
        jobs = Queue()  # instantiate job queue
        dispLock = Lock()  # instantaite display lock (see previous example)

        # instantiate N slave processes
        for procID in range(DEALER_P):
            pool.append(Process(target=deal, args=(procID, jobs, dispLock)))

        # populate the job queue
        for jobID in range(DEALER_J):
            jobs.put({'jobID': jobID})

        # start the slaves
        for slave in pool:
            slave.start()

        # wait for the slaves to finish processing
        for slave in pool:
            slave.join()

        def stats(self, currency):
            pass
