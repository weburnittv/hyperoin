from core.Services.base import ExchangeService, Ticker

TRACKER_CONFIG = {'threshold': 0.2, 'budget': 5}


class Tracker:
    def __init__(self, client, market, config):
        self.client = client
        self.market = market
        self.config = config
        self.ticker = None
        self.running = True

    def run(self, broadcast=lambda tracker, ticker: print('Processed %s:%s' % tracker.market, ticker.last)):
        while self.running:
            ticker = self.client.ticker(self.market)
            if self.ticker:
                self.__stats(ticker, broadcast)
                self.ticker = ticker

    def stop(self):
        self.running = False

    def __stats(self, ticker, broadcast):
        if ticker.last / self.ticker.last > 0.2:
            self.client.buy(self.config['budget'], self.market, ticker.ask)
            broadcast(self, ticker)
