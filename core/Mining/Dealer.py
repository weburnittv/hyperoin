from core.Services.base import ExchangeService


class Dealer:
    def __init__(self, currency, amount):
        self.currency = currency
        self.amount = amount

    def buy(self, exchange: ExchangeService, bid):
        return exchange.buy(self.amount, self.currency, bid)

    def sell(self, exchange: ExchangeService, bid):
        return exchange.buy(self.amount, self.currency, bid)
