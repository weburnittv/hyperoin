FROM jcdemo/flaskapp

ADD ./ /home/app/
WORKDIR /home/app/
RUN apk update && \
    apk add --virtual build-deps gcc python3-dev musl-dev && \
    apk add postgresql-dev
RUN apk add --update bash && rm -rf /var/cache/apk/*
RUN pip install --upgrade pip
RUN pip install -r dependency.txt
RUN pip install -e .
